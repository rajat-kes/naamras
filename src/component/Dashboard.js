import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Image } from "react-native";
export default class You extends Component {
  render() {
    return (
      <View style={styles.container}>
        {/* For Heading View */}
        <View style={{ height: 50, backgroundColor: "#4a4a4a" }}>
          <Text style={{ fontSize: 40, alignSelf: "center" }}>Naamras</Text>
        </View>
        {/*Box 1 Coding start here */}
        <View style={[styles.Box1, styles.Heading]}>
          <Text style={{ fontSize: 30, color: "#aaaaaa" }}>
            Welcome to DashBoard
          </Text>
        </View>

        {/*Box 2 Coding start here */}
        <View style={[styles.Box2, styles.Dog]}>
          {/*Row 1 */}
          <View style={{ flexDirection: "column" }}>
            <Image
              style={{ width: 70, height: 70 }}
              source={require("../image/logo/Schedule.png")}
            />
            <Text style={{ alignSelf: "center" }}>Schedule</Text>
          </View>
          <View style={{ flexDirection: "column" }}>
            <Image
              style={{ width: 70, height: 70 }}
              source={require("../image/logo/Hall_Map.png")}
            />
            <Text style={{ alignSelf: "center" }}>Hall_Map</Text>
          </View>
          <View style={{ flexDirection: "column" }}>
            <Image
              style={{ width: 70, height: 70 }}
              source={require("../image/logo/Gallery.png")}
            />
            <Text style={{ alignSelf: "center" }}>Gallery</Text>
          </View>
        </View>

        <View style={[styles.Box2, styles.Cat]}>
          {/*Row 2 */}
          <View style={{ flexDirection: "column" }}>
            <Image
              style={{ width: 70, height: 70 }}
              source={require("../image/logo/Sewa.png")}
            />
            <Text style={{ alignSelf: "center" }}>Sewa</Text>
          </View>
          <View style={{ flexDirection: "column" }}>
            <Image
              style={{ width: 70, height: 70 }}
              source={require("../image/logo/Transport.png")}
            />
            <Text style={{ alignSelf: "center" }}>Transport</Text>
          </View>
          <View style={{ flexDirection: "column" }}>
            <Image
              style={{ width: 70, height: 70 }}
              source={require("../image/logo/Calendar.png")}
            />
            <Text style={{ alignSelf: "center" }}>Calendar</Text>
          </View>
        </View>

        <View style={[styles.Box2, styles.Rat]}>

          {/*Row 3 */}
          <View style={{ flexDirection: "column" }}>
            <Image
              style={{ width: 70, height: 70 }}
              source={require("../image/logo/Exhibition.png")}
            />
            <Text style={{ alignSelf: "center" }}>Exhibition</Text>
          </View>
          <View style={{ flexDirection: "column" }}>
            <Image
              style={{ width: 70, height: 70 }}
              source={require("../image/logo/Events.png")}
            />
            <Text style={{ alignSelf: "center" }}>Events</Text>
          </View>
          <View style={{ flexDirection: "column" }}>
            <Image
              style={{ width: 70, height: 70 }}
              source={require("../image/logo/Personalities.png")}
            />
            <Text style={{ alignSelf: "center" }}>Personalities</Text>
          </View>
        </View>

        <View style={[styles.Box2, styles.Bob]}>
          {/*Row 4 */}
          <View style={{ flexDirection: "column" }}>
            <Image
              style={{ width: 70, height: 70 }}
              source={require("../image/logo/Sponsors.png")}
            />
            <Text style={{ alignSelf: "center" }}>Sponsors</Text>
          </View>
          <View style={{ flexDirection: "column" }}>
            <Image
              style={{ width: 70, height: 70 }}
              source={require("../image/logo/Blog.png")}
            />
            <Text style={{ alignSelf: "center" }}>Blog</Text>
          </View>
          <View style={{ flexDirection: "column" }}>
            <Image
              style={{ width: 70, height: 70 }}
              source={require("../image/logo/Contact_Us.png")}
            />
            <Text style={{ alignSelf: "center" }}>Contact Us</Text>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5FCFF"
  },
  Box1: {
    flex: 1
  },
  Heading: {
    alignItems: "center"
  },
  Box2: {
    flex: 2
  },
  Dog: {
    flexDirection: "row",
    justifyContent: "space-around"
  },
  Cat: {
    flexDirection: "row",
    justifyContent: "space-around"
  },
  Rat: {
    flexDirection: "row",
    justifyContent: "space-around"
  },
  Bob: {
    flexDirection: "row",
    justifyContent: "space-around"
  }
});
